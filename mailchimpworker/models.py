from django.db import models


class Subscriber(models.Model):
    email = models.EmailField('Почта', unique=True, db_index=True)
    name = models.CharField('Имя', max_length=100, blank=True, null=True)
    status = models.BooleanField('Отправлено в Mailchimp', default=False)

    def __str__(self):
        return '{}, {}'.format(self.email, self.name)
