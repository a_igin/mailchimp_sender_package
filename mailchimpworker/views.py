from django.shortcuts import render
from .models import *


def new_subscriber(request):
    data_email = request.POST.get('EMAIL')
    unique_check = Subscriber.objects.filter(email=data_email)
    if not len(unique_check):
        Subscriber.objects.create(email=data_email)
    return render(request, 'home/form_thanks.html', {})
