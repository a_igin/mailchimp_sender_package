import requests
import json
import hashlib
from django.conf import settings
from django.core.management.base import BaseCommand
from ... import models


class Command(BaseCommand):
    help = "Add new subscribers to MailChimp."

    def handle(self, *args, **options):
        z = 0
        for i in models.Subscriber.objects.filter(status=False):
            post_mailchimp(i.email)
            i.status = True
            i.save()
            z += 1
        print('Added {} emails'.format(z))


def post_mailchimp(email):
    api_postfix = 'lists/{}/members'.format(settings.MAILCHIMP_LIST_ID)
    payload = {
        'email_address': email,
        'status': 'subscribed',
        'interests': {settings.MAILCHIMP_INTEREST_ID: True}
    }
    result = requests.post(settings.MAILCHIMP_API_ROOT + api_postfix, data=json.dumps(payload),
                           auth=('anystring', settings.MAILCHIMP_API_KEY)).json()

    try:
        if result['title'] and result['title'] == 'Member Exists':
            subscriber_hash = hashlib.md5()
            subscriber_hash.update(email.lower().encode('utf-8'))
            result_hash = subscriber_hash.hexdigest()

            api_postfix = 'lists/{}/members/{}'.format(settings.MAILCHIMP_LIST_ID, result_hash)
            user_info_json = requests.get(settings.MAILCHIMP_API_ROOT + api_postfix,
                                          auth=('anystring', settings.MAILCHIMP_API_KEY)).json()
            user_interests = user_info_json['interests']
            user_interests.update({settings.MAILCHIMP_LIST_ID: True})
            payload = {'interests': user_interests}
            requests.patch(settings.MAILCHIMP_API_ROOT + api_postfix, data=json.dumps(payload),
                           auth=('anystring', settings.MAILCHIMP_API_KEY))
    except KeyError:
        pass
