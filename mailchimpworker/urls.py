from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^new_subscriber$', views.new_subscriber, name='new_subscriber'),
]
